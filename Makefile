DOCKER_PRJ = odoo
SRCDIR = ./additional_addons/


BRANCH=master

SRC_PACKAGES = $(wildcard $(SRCDIR)*)

PACKAGES = \
	odoo_demo

odoo_demo: GIT_URL=git@git.abstract.it:abstract-openerp/odoo_demo.git

.PHONY: build bootstrap clone update run stop destroy shell


all: bootstrap run

$(PACKAGES):
	@if [ ! -d $(SRCDIR) ]; then mkdir $(SRCDIR); fi
	@if [ ! -d $(SRCDIR)$@ ]; then git -C $(SRCDIR) clone -b $(BRANCH) $(GIT_URL); fi \


clone: $(PACKAGES)

update:
	@for pkg in $(SRC_PACKAGES) ; do \
		echo Update $$pkg ; \
		git -C $$pkg pull ; \
	done


build:
	@docker-compose build $(DOCKER_PRJ)


bootstrap: clone build


run:
	@docker-compose run --rm --service-ports $(DOCKER_PRJ)


stop:
	@docker-compose stop


destroy: stop
	@docker-compose rm -f -v


shell:
	@docker-compose run --rm --service-ports $(DOCKER_PRJ) /bin/bash
